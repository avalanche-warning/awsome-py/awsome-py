################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# Unit test for the SMET parser.

import awio
from awsmet import SMETParser
import os
import time

def ptdiff(stime):
    elapsed = awio.iso_timediff(stime, ns=True)
    print(elapsed)

errors = 0
csd = awio.get_scriptpath(__file__) # script directory
fsmet = os.path.join(csd, "MST96.smet") # SNOWPACK example file

stime = time.time_ns()
smet = SMETParser(fsmet)
ptdiff(stime)
stime = time.time_ns()
df = smet.df()
df_at_beginning = df.copy()
ptdiff(stime)
stime = time.time_ns()
df = smet.df()
ptdiff(stime)

if df["TA"].max() != 287.55:
    print("Mismatch of max value")
    errors += 1
if df["HS"].sum() != 10075.383:
    print("Mismatch of sum")
    errors += 1

header = {}
header["station_id"] = "TSTA"
header["station_name"] = "Test station"
stime = time.time_ns()
SMETParser.write_df(df, "./out.smet", header=header)
ptdiff(stime)
fsz = os.path.getsize("./out.smet")
if fsz != 1138239:
    print("Output file size mismatch")
    errors += 1

df_at_end = df.copy()
if not df_at_beginning.equals(df_at_end):
    print("DataFrame was modified")
    errors += 1

ts_ref = ("1995-10-30T00:30", "1996-06-17T00:00")
ts = SMETParser.get_timespan(fsmet)
if ts_ref != ts:
    print("Time span mismatch")
    errors += 1

print(f"There were {errors} error(s) manipulating SMET content.")
exit(0 if errors == 0 else 1)
