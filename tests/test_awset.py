################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# Unit test for XML file parser.

import awio
import awset
import os

errors = 0
csd = awio.get_scriptpath(__file__) # script directory

# test single setting
setting = awset.get(["model", "run", "days"], "test_awset", csd)
if setting != "mon,thu":
	errors = errors + 1

# test list setting
listsetting = awset.getlist(["model", "module", "data"], "test_awset", csd)
if listsetting != ["STAT1", "STAT2"]:
	errors = errors + 1

# test attributes
attribute = awset.getattribute(["model", "output", "pdf"], "enabled", "test_awset", csd)
if attribute != "true":
	errors = errors + 1

# test CRS
crs = awset.get_crs("test_awset", "epsg", toolchain="model", cfgpath=csd)
if crs != 32633:
	errors = errors + 1

# test elevation bands
lower_limits, upper_limits, generate_flags, band_names = awset.get_elev_bands("test_awset", "model", cfgpath=csd)
if all(band_names[generate_flags] != 'TL'):
	errors = errors + 1
if lower_limits[0] != 1700:
	errors = errors + 1
	
print(f"There were {errors} error(s) when parsing the XML file.")
exit(0 if errors == 0 else 1)
