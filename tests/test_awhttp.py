################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# Unit test for fetching web content.

from awhttp import download, write
import awio
import filecmp
import os

_create_ref = False # write out the reference html?

csd = awio.get_scriptpath(__file__) # script directory
errors = 0

response = download("https://lawinen.report/simple/2022-01-01/de.html")

ref_filepath = os.path.join(csd, "test_awhttp_ref.html")
content_filepath = os.path.join(csd, "test_awhttp.html")

if _create_ref:
	write(response, ref_filepath)
	exit(0)

write(response, content_filepath)

if not filecmp.cmp(ref_filepath, content_filepath, shallow=False):
	errors = errors + 1

print(f"There were {errors} error(s) when fetching web content.")
exit(0 if errors == 0 else 1)
