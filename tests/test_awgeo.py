import pyproj
import awgeo.geoconverter

proj_string = pyproj.Proj("EPSG:4326").definition_string()
crs = awgeo.geoconverter._epsg_code_auto(proj_string)
print(crs)
