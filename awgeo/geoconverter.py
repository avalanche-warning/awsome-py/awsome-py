################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import os
import osgeo.osr as osr
from pathlib import Path
import rasterio
import rasterio.warp
import re
import subprocess

def raster2asc(fname):
    path, ext = os.path.splitext(fname)
    if (ext in [".tif", ".tiff", ".gtif", ".gtiff"]):
        tif2asc(fname)
    else:
        sys.exit(f"[E] Can not convert raster file from unknown format '{ext}'")

def tif2asc(fname, skip_existing=True):
    path, _ = os.path.splitext(fname)
    outfile = Path(path + ".asc")
    if skip_existing and outfile.exists():
        print(f"[i] Skipping conversion of raster (file exists): {fname}.asc")
    else:
        subprocess.run(["gdal_translate", "-of", "AAIGrid", f"{fname}", path + ".asc"])

def reproject(src_crs_epsg: int, dst_crs_epsg: int, datafile: str, outfile: str, nodata_value: int=-999):
    """Reproject a raster data set and output result to file system.
    The input crs must be set (to account for ASCII files e. g.) and will be applied to the source
    regardless of wheter it has a proper crs already.

    Args:
        src_crs_epsg: EPSG code of datafile.
        dst_crs_epsg: EPSG code of output (system to transform to)
        datafile: The raster data file.
        outfile: Path of output file.
        nodata_value: These values will be set to numpy.nan
    """

    dst_crs = f"EPSG:{dst_crs_epsg}"
    with rasterio.open(datafile, "r+") as src:
        src.crs = rasterio.crs.CRS.from_epsg(src_crs_epsg)

        # calculate output transform matrix:
        dst_transform, width, height = rasterio.warp.calculate_default_transform(
            src.crs, dst_crs, src.width, src.height, *src.bounds)

        dst_meta = src.meta.copy()
        dst_meta.update(
            {
                "crs": dst_crs,
                "transform": dst_transform,
                "width": width,
                "height": height,
                "nodata": nodata_value
            }
        )

        with rasterio.open(outfile, "w", **dst_meta) as dst:
            for band in range(1, src.count + 1):
                rasterio.warp.reproject(
                    source=rasterio.band(src, band),
                    destination=rasterio.band(dst, band),
                    src_transform=src.transform,
                    src_crs=src.crs,
                    dst_transform=dst_transform,
                    dst_crs=dst_crs,
                    resampling=rasterio.warp.Resampling.nearest
                )

def reproject_bounds(src_crs: rasterio.crs.CRS, dst_crs: int, bounds):
    new_crs = f"EPSG:{dst_crs}"
    bbox = rasterio.warp.transform_bounds(src_crs, rasterio.crs.CRS({"init": new_crs}), *bounds) # left, bottom, right, top
    bbox = [ [bbox[1], bbox[0]], [bbox[3], bbox[2]] ] # [lat_min, lon_min], [lat_max, lon_max] (as expected by folium)
    return bbox

def get_epsg_code(filename: str) -> int:
    # First check if we have it specified in the file name:
    code = re.search(r"(.*epsg)([0-9]+)([^0-9]+)", filename)
    if code:
        return code.group(2)

    # Then check if there is a .prj file associated:
    projfile = os.path.splitext(filename)[0] + ".prj"
    if os.path.exists(projfile):
        code = _epsg_code_autofile(projfile)
        if code: # the crs string has a well dfined epsg within
            return code
        # If we could not find it still we handle some cases manually:
        with open(projfile) as proj:
            crs = proj.read()
            if "MGI_Austria_GK_West" in crs:
                return 31254
    # Now see if rasterio can fetch it:
    dem = rasterio.open(filename)
    crs = dem.crs.items()
    if "init" in [item[0] for item in crs]:
        return crs.mapping["init"]
    elif ("proj", "lcc") in crs and ("lat_0", 47.5) in crs and ("lat_1", 46) in crs:
        return "31287" # pretty sure it's MGI Austrian Lambert

    return None

def _epsg_code_autofile(crs_file: str):
    """Try to read EPSG code from a file."""
    with open(crs_file) as crs:
        return _epsg_code_auto(crs.read())

def _epsg_code_auto(crs_string: str):
    """Try to read EPSG code from a string.

    Of course, not all coordinate systems have an EPSG code and even if
    they do the 'authority' field may not be filled.
    """
    spatial = osr.SpatialReference()
    spatial.ImportFromProj4(crs_string)
    spatial.AutoIdentifyEPSG()
    epsg_code = spatial.GetAuthorityCode(None)
    return epsg_code
