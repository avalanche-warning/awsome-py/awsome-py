################################################################################
# Copyright 2023 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import pyproj
import shapely
import shapely.ops
import geopandas as gpd

def area_poly_wkt_4326(polygon: str) -> float:
    """	Calculate area of a polygon given in well known text format.
	
    Arguments:
        polygon (str): Polygon given as "POLYGON ((lon, lat..." string
    """
    identifier = "POLYGON (("
    polygon = polygon[polygon.upper().find(identifier) + len(identifier):-3]
    coords = polygon.split(", ")
    coords = [cc.split() for cc in coords]
    feature = shapely.geometry.Polygon(coords)
    
	# To get the area we need to reproject from spherical to some Cartesian coordinates:
    wgs84 = pyproj.CRS("EPSG:4326")
    aea = pyproj.crs.coordinate_operation.AlbersEqualAreaConversion(feature.bounds[1], feature.bounds[3])
    aea = pyproj.crs.ProjectedCRS(conversion=aea)
    projector = pyproj.Transformer.from_crs(wgs84, aea, always_xy=True).transform
    geometry = shapely.ops.transform(projector, feature)

    return geometry.area # m^2


def find_polygon_id(geojson_path, easting, northing):
    """
    Find the id of the polygon that contains the given coordinates. 
    CRS of geojhson file and easting northing coordinates mus be identical!

    Parameters:
    ----------
    geojson_path : str
        Path to the GeoJSON file.
    easting : float
        The easting (x-coordinate) of the point.
    northing : float
        The northing (y-coordinate) of the point.

    Returns:
    -------
    int or None
        The id of the polygon that contains the point, or None if no polygon contains it.
    """
    gdf = gpd.read_file(geojson_path)
    point = shapely.geometry.Point(easting, northing)
    matching_polygon = gdf[gdf.geometry.contains(point)]
    
    if not matching_polygon.empty:
        return matching_polygon.iloc[0]['id']
    else:
        return None

