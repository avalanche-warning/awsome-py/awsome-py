################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

from awhttp import download, write
import awio
import awset
from geopy import distance
import glob
import json
import os
import time

def dl_stations_list(outfile="./ogd.geojson"):
    api_url = "https://wiski.tirol.gv.at/lawine/produkte/ogd.geojson"
    if os.path.exists(outfile):
        return # TODO: re-download if old
    else:
        response = download(api_url, throw=True)
        write(response, outfile)

def find_closest_station(poi_location, stat_json, station_type="snow", has_field=None):
    """coords in lon/lat"""
    with open(stat_json) as statfile:
        data = json.load(statfile)

    min_station = None
    min_dist = None
    found_field = False
    for feature in data["features"]:
        props = feature["properties"]
        if props["operator"] != "LWD Tirol":
            continue
        try:
            stat_id = props["LWD-Nummer"]
        except KeyError:
            continue
        if stat_id == "PLACEHOLDER":
            continue
        if station_type.lower() == "wind":
            if not stat_id.endswith("1"):
                continue
        if has_field:
            try:
                _ = props[has_field]
            except KeyError:
                continue
        geo = feature["geometry"]
        coords = geo["coordinates"]
        coords = coords[:2] # disregard elevation
        latlon = (poi_location[1], poi_location[0])
        stat_name = props["name"]

        dist = distance.distance(latlon, coords).km
        if not min_dist or dist < min_dist:
            min_dist = dist
            min_station = feature

    return min_station, min_dist

def list_aw_domains(domains_path: str=awset.DEFAULT_DOMAIN_PATH):
    """Get list of settings XML basenames in a settings folder."""
    domains_path = os.path.expanduser(domains_path)
    domains = glob.glob(f"{domains_path}/*.xml")
    domains.sort()
    domains = [os.path.basename(dom) for dom in domains]
    domains = [dom[:-4] for dom in domains]
    return domains

def get_pois(domain: str):
    """Get points of interest defined in a domain settings file."""
    if not awset.exists(["report", "pois"], domain):
        return []
    pois = awset.nodelist_dict(["report", "pois"], domain=domain)
    return pois["poi"] # list with each individual poi

def select_full_poi(domain: str, poi_name: str) -> dict:
    """For a POI name look up its ID and coordinates."""
    pois = get_pois(domain)
    if not pois: # no pois in domain xml
        return {}
    if not poi_name: # init app without poi --> choose one
        poi = pois[0]
    else:
        for domain_poi in pois:
            if domain_poi["locationName"].lower() == poi_name.lower():
                poi = domain_poi # select dict item with full info about POI
                break
    return poi

def get_last_wrffile(domain: str, wrf_out_path="~wrf/wrf/output") -> str:
    """Retrieve the newest WRF output file for the domain."""
    wrf_out_path = os.path.expanduser(wrf_out_path)
    path = os.path.join(wrf_out_path, f"*_{domain}.nc")
    wrffiles = glob.glob(path)
    if wrffiles:
        wrffiles.sort()
        return wrffiles[-1] # latest one
    return ""

