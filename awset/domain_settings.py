################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import os
import pyproj
import numpy as np
from typing import Literal
import xml.etree.ElementTree as ET

from .awsome_settings import aenv

def _domloc():
    """Return location of domain xml files."""
    return aenv("AWSOME_DOMAINS", defaults=True)

def _tobool(string: str) -> bool:
    trues = ["1", "true", "t", "yes", "y"]
    return string.lower() in trues

def _get_node(setting: list, domain: str, cfgpath: str):
    cfgpath = os.path.expanduser(cfgpath)
    tree = ET.parse(os.path.join(cfgpath, domain + ".xml"))
    root = tree.getroot()

    setting_path = "." + "/".join(setting)
    key = root.findall(setting_path)
    return key

def get(setting: list, domain: str, cfgpath: str=None,
        throw: bool=False, warn: bool=True, default=None):
    if not cfgpath:
        cfgpath = _domloc()

    key = _get_node(setting, domain, cfgpath)
    if not key:
        if throw:
            raise Exception("[E] XML setting not found: " + "/".join(setting))
        else:
            return default
    if len(warn and key) > 1:
        print("[W] XML setting not unique: " + "/".join(setting))
    if not key[0].text:
        return default
    else:
        return key[0].text

def getbool(setting: list, domain: str, cfgpath: str=None,
        throw: bool=False, warn: bool=True, default=None):
    if not cfgpath:
        cfgpath = _domloc()
    text = get(setting, domain, cfgpath, throw, warn, default)
    if not text:
        return None
    return _tobool(text)

def getlist(setting: list, domain: str, cfgpath: str=None, default=None):
    if not cfgpath:
        cfgpath = _domloc()
    key = get(setting, domain, cfgpath)
    if not key:
        return default
    else:
        return key.split()

def getattribute(setting: list, attribute: str, domain: str,
        cfgpath: str=None, default=None):
    if not cfgpath:
        cfgpath = _domloc()
    key = _get_node(setting, domain, cfgpath)
    if not key:
        return default
    else:
        try:
            return key[0].attrib[attribute]
        except KeyError:
            return default

def exists(setting: list, domain: str, cfgpath: str=None):
    if not cfgpath:
        cfgpath = _domloc()
    key = _get_node(setting, domain, cfgpath)
    if not key:
        return False
    return True

def nodelist_dict(setting: list, domain: str, cfgpath: str=None) -> dict:
    """Parse a node's children into a dictionary.

    The passed "root" node's child elements are put into a dictionary
    keyed by their tag. This outer dictionary's values are the respective
    settings found in that child node.
    If the "root" node's tag appears more than once, the diconary contains
    a list of subdictionaries for the settings.
    """
    if not cfgpath:
        cfgpath = _domloc()
    outer_dict = {}
    parent = _get_node(setting, domain, cfgpath)
    if not parent:
        return {}
    else:
        parent = parent[0]
    for item in parent:
        if not item.tag in outer_dict:
            outer_dict[item.tag] = []
        item_dict = {}
        for child in item:
            item_dict[child.tag] = child.text
        outer_dict[item.tag].append(item_dict)
    return outer_dict

def is_domain_enabled(module: str, domain: str) -> bool:
    """Check if a module is enabled.

    This function returns True if the node <module enabled="true">
    is found.
    """
    enabled = getattribute([module], "enabled", domain)
    if enabled is None: # does not exist
        return False
    return _tobool(enabled)

def is_enabled(setting: list, domain: str) -> bool:
    """Check if a setting is enabled."""
    enabled = getattribute(setting, "enabled", domain)
    if enabled is None: # does not exist
        return False
    return _tobool(enabled)

def get_crs(domain: str, return_type: Literal['crs', 'epsg', 'wkt_string', 'proj4_string']='crs', 
            cast_type: Literal['nowcast', 'forecast']='nowcast', toolchain: str='gridded-chain', 
            fallback_to_nwp_dem: bool=True, fallback_to_netcdf: bool=False, throw: bool=True,
            **kwargs):
    """
    Retrieve the Coordinate Reference System (CRS) for the meteo data of a given domain.
    
    The function searches for the CRS information in the domain.xml config file and can return the CRS in multiple formats.
    If the CRS is not found, the function can optionally fallback to checking the corresponding NetCDF file created through 
    the nwp_provider for CRS information.
    
    Parameters
    ----------
    domain : str
        The domain for which the CRS is being requested.
    
    return_type : {'crs', 'epsg', 'wkt_string', 'proj4_string'}
        The desired format of the returned CRS:
        - 'crs' (default): returns a pyproj.CRS object.
        - 'epsg': returns the EPSG code, or None if not available.
        - 'wkt_string': returns the Well-Known Text (WKT) representation of the CRS.
        - 'proj4_string': returns the PROJ4 string representation of the CRS.
        
    cast_type : {'nowcast', 'forecast'}
        The type of meteorological data to retrieve the CRS for (i.e., it will search the xml paths
        {toolchain}/meteo/{cast_type}/crs and {toolchain}/meteo/crs.)
        
    toolchain : str
        The toolchain used to query the CRS in the XML configuration.

    fallback_to_nwp_dem : bool
        If True, and if the CRS is not found in the XML, the function attempts to load the NWP DEM
        associated with the domain and toolchain and retrieve the CRS from the DEM's crs attribute.
        
    fallback_to_netcdf : bool
        If True, and if the CRS is not found in the XML, the function attempts to load a NetCDF file
        associated with the domain and toolchain and retrieve the CRS from the file's crs attribute.

    throw : bool
        If True, the function raises a ValueError if no CRS can be found. 
        If False, it returns None instead of raising an exception.

    **kwargs : dict
        Additional arguments passed to the `get` function for querying XML paths.

    Returns
    -------
    crs : pyproj.CRS, int, str, or None
        Returns None if `throw=False` and no CRS could be found.

    Raises
    ------
    ValueError
        If the CRS cannot be found and `throw=True`.

    pyproj.exceptions.CRSError
        If the CRS string found cannot be parsed by pyproj.
    """
    crs_string = get([toolchain, 'meteo', cast_type, 'crs'], domain, **kwargs) or \
           get([toolchain, 'meteo', 'crs'], domain, **kwargs)
    
    if crs_string is None and fallback_to_nwp_dem:
        nwp_demfile_path = get([toolchain, 'meteo', 'dem', 'nc_file'], domain, throw=False, **kwargs)
        import xarray as xr
        dem_nwp = xr.open_dataset(nwp_demfile_path)
        if 'crs' in dem_nwp.attrs:
            crs_string = dem_nwp.attrs['crs']
    if crs_string is None and fallback_to_netcdf:
        import nwp_provider as awnwp
        ds_nwp = awnwp.open_dataset(domain, toolchain, cast_type, update_dataset=False,
            prefer_nc_over_db=True)
        if 'crs' in ds_nwp.attrs:
            crs_string = ds_nwp.attrs['crs']
    if crs_string is None:
        if throw:
            raise ValueError("Cannot find meteo Coordinate Reference System.")
        return None
    crs = pyproj.CRS(crs_string)
    if return_type == 'crs':
        return crs
    elif return_type == 'epsg':
        return crs.to_epsg()
    elif return_type == 'wkt_string':
        return crs.to_wkt()
    elif return_type == 'proj4_string':
        return crs.to_proj4()

def get_elev_bands(domain: str, toolchain: str='gridded-chain', **kwargs):
    """
    Get elevation band settings from domain.xml file and convert them to NumPy arrays.

    Returns:
    - tuple of NumPy arrays: (lower_limits, upper_limits, generate_flags, band_names)
    """
    setting_path = [toolchain, 'elevation_bands']
    bands_data = nodelist_dict(setting_path, domain, **kwargs)['band']

    lower_limits = np.array([band['lower_limit'] for band in bands_data], dtype=np.float64)
    upper_limits = np.array([band['upper_limit'] for band in bands_data], dtype=np.float64)
    generate_flags = np.array([_tobool(band.get('enabled', 'true')) for band in bands_data])
    band_names = np.array([band['name'] for band in bands_data])
    return lower_limits, upper_limits, generate_flags, band_names
