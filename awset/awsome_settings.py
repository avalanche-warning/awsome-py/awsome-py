################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import os

def awsome_env(key: str, defaults: bool=False):
    """Get an AWSOME setting.

    Toolchain settings are in the domain files, here we get system specific
    settings like the host name, code base location, ... cf. defaults.
    They are usually set as environment variables and this function is a
    safe-guarded way to retrieve them.

    Arguments:
        key: Name of environment variable (e. g. 'AWSOME_HOME').
        defaults: If n/a pick some defaults? Should hardly come to this...
    """
    val = None
    try:
        val = os.environ[key]
    except KeyError:
        pass
    if not val:
        val = _get_from_envfile(key)
    if not val and defaults:
        val = get_default(key)
    return val

def aenv(key: str, defaults: bool=False):
    """Convenience shortcut call."""
    return awsome_env(key, defaults)

def _get_from_envfile(key: str):
    """Try to read value from environment file.

    This file is created by the installer and puts the variables
    into new login shells. Here we parse them manually.
    """
    _fenv = "/opt/awsome/.env" # global environment variables

    with open(_fenv, "r") as envfile:
        lines = envfile.readlines()
    for line in lines:
        if line.strip("\n").startswith(f"{key}="):
            val = line[len(key) + 1:].strip()
            return val
    return None

def get_default(key: str):
    """Safe-guard function to get default values.

    The defaults should have been put to the environments
    file by the installer, so this should only be reachable
    when working outside of an AWSOME installation.
    """
    defaults = {
        "AWSOME_BASE": "/opt/awsome",
        "AWSOME_CODE": "/opt/awsome/code",
        "AWSOME_DOMAINS": "/opt/awsome/domains",
        "AWSOME_HOME": "/home",
        "AWSOME_HOST": "avalanche.report",
        "AWSOME_SECRETS": "/opt/awsome/secrets.ini",
        "AWSOME_STORAGE": "/mnt/storage-box",
        "AWSOME_VENVS": "/opt/awsome/venvs",
        "AWSOME_WWW": "/var/www/html"
    } # changes here probably need to be reflected in server-setup/.env
    try:
        return defaults[key]
    except KeyError:
        return None

