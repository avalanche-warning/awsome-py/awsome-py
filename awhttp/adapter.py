################################################################################
# Copyright 2022 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This is a central HTTPAdapter to have reasonable default
# behaviour from everywhere.
# Michael Reisecker, 2020-12-13

import gzip
import io
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

STD_TIMEOUT = 5 #seconds
STD_RETRIES = 5

class StdHTTPAdapter(HTTPAdapter):
    def __init__(self, *args, **kwargs):
        retry_strategy = Retry(
            total = STD_RETRIES, #retry this many times
            read = STD_RETRIES,
            connect = STD_RETRIES,
            status_forcelist = [429, 500, 502, 503, 504, 404], #HTTP codes to retry on
            allowed_methods = ["HEAD", "GET", "PUT", "DELETE", "OPTIONS", "TRACE"], #methods to retry on
            backoff_factor = 1 #{backoff factor} * (2 ** ({number of total retries} - 1)) <-- time delay
        )
        self.timeout = STD_TIMEOUT
        if "timeout" in kwargs:
            self.timeout = kwargs["timeout"]
            del kwargs["timeout"]
        super().__init__(max_retries=retry_strategy, *args, **kwargs)

    def send(self, request, **kwargs):
        timeout = kwargs["timeout"]
        if timeout is None: #inject default timeout if not given
            kwargs["timeout"] = self.timeout
        return super().send(request, **kwargs)

def download(url, decompress=None, throw=False, token=None, **kwargs):
    adapter = StdHTTPAdapter()
    web = requests.Session()
    web.mount("http://", adapter)
    if token: # authentication via access token
        web.headers.update({"Authorization": f"Bearer {token}"})

    # follow moved resources and get new link:
    response = web.get(url, allow_redirects=False)
    while response.status_code in (301, 302, 303, 307):
        url = response.headers["Location"]
        response = web.get(url, allow_redirects=False)

    response = web.get(url, **kwargs)
    if throw:
        response.raise_for_status()
    return response
    
def write(response, outfilepath, decompress=None):
    if not decompress:
        with open(outfilepath, mode="wb") as outfile:
            outfile.write(response.content)
    elif decompress == "gzip":
        fcomp = io.BytesIO(response.content)
        fdecomp = gzip.GzipFile(fileobj=fcomp)
        with open(outfilepath, "wb") as outfile:
            outfile.write(fdecomp.read())
