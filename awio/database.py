################################################################################
# Copyright 2024 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This module is not automatically loaded because of heavy dependencies.

import gridfs

def _pad_b64_data(data: str, padchar: str="="):
    """Pads base64 encoded data to lengths of multiples of 4."""
    if len(data) % 4 == 0:
        return data
    npad = -len(data) % 4
    data += padchar * npad
    return data

def key_exists(fs, key: str, case_insensitive: bool=True):
    options = "i" if case_insensitive else None
    exists = fs.exists({"filename": {"$regex": f"^{key}$", "$options": options}})
    return exists
