################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import datetime
import numpy as np
import pandas as pd
import time

def validate_date(datestr, format):
    """Try if a date string has a specific format."""
    try:
        datetime.datetime.strptime(datestr, format)
        return True
    except ValueError:
        return False

def choose_display_dates(dates: list):
    """Select a few dates to display from a list.

    WRF plots for example will usually not want to display
    every single available time step.
    """
    chosen = []
    timespan = dates[-1] - dates[0]
    twodays = np.timedelta64(2, "D")
    fourdays = np.timedelta64(4, "D")

    if twodays < timespan < fourdays:
        for ii in range(len(dates)):
           pdate = pd.Timestamp(dates[ii])
           if pdate.hour in [6, 12, 18]:
               chosen.append((dates[ii], ii))
    else:
        for ii in range(len(dates)):
            chosen.append((dates[ii], ii))

    return chosen

def iso_timediff(stime, ns=False, precision=9):
    """Helper function to pretty-print a timed function call.

    Using nanoseconds precision returns the number of seconds,
    otherwise hh:mm:ss is returned.
    Call with stime = time.time() or time.time_ns()
    """
    if ns:
        elapsed = (time.time_ns() - stime) / (10 ** 9)
        return f"{elapsed:.{precision}f} s"
    else:
        elapsed = time.time() - stime
        hh, remainder = divmod(elapsed, 3600)
        mm, ss = divmod(remainder, 60)
        fsecs = f"{ss:.1f}"
        ii, dd = fsecs.split(".")
        ii = ii.zfill(2) # pad without taking separator into account
        return f"{hh:02.0f}:{mm:02.0f}:{ii}.{dd}"

def closest_index(dates: list, request_date):
    """Given a list of dates find the index closest to a certain time stamp."""
    request_date = pd.to_datetime(request_date)
    idx_closest = min(range(len(dates)),
        key=lambda idx: abs(dates[idx] - request_date))
    return idx_closest

