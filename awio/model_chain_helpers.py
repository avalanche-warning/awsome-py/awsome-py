import os
import configparser
import pandas as pd
from typing import Literal
import time
from datetime import datetime
import numpy as np
import subprocess

from awset import get, exists
import awio
from awsmet import smet_parser

_upload_script_path = os.path.expandvars('$AWSOME_BASE/visualization/upload/upload.py')


def get_config(domain:str, toolchain:Literal['gridded-chain', 'profile-forecast'], config_runtime:bool=True)->configparser.ConfigParser:

    config = configparser.ConfigParser()
    config_path = os.path.expanduser(f"~snowpack/{toolchain}/input/forecast_{'runtime_' if config_runtime else ''}{domain}.ini")
    if os.path.exists(config_path):
        config.read(config_path)
    else:
        raise FileNotFoundError("Has the domain config been initialized already?")
    config = awio.expanduser(config)

    return config

def get_input_smet(config, vstation_id:str|None=None, cast_type='nowcast'):

    if vstation_id is None:
        df_vstations = pd.read_csv(config.get('Paths', '_vstations_csv_file_runtime'))
        vstation_id = f"VIR{df_vstations.loc[df_vstations['error'] == 0, 'vstation'][0]}"
    else:
        err_msg = ''
        if 'VIR' not in vstation_id:
            err_msg += "Your vstation_id most likely misses 'VIR'..\n"

    try:
        smet_path = f"{os.path.join(config.get('Paths', f'_smet_files_dir_{cast_type}'), str(vstation_id))}.smet"
        smet = smet_parser.SMETParser(smet_path)
    except FileNotFoundError as e:
        error_details = err_msg + f"\nOriginal error: {str(e)}"
        raise FileNotFoundError(f"Error loading SMET file for vstation_id '{vstation_id}'. {error_details}")
    
    return smet

def get_output_smet(config, vstation_id:str|None=None):

    if vstation_id is None:
        df_vstations = pd.read_csv(config.get('Paths', '_vstations_csv_file_runtime'))
        vstation_id = f"VIR{df_vstations.loc[df_vstations['error'] == 0, 'vstation'][0]}A"
    else:
        err_msg = '\n'
        if 'VIR' not in vstation_id:
            err_msg += "Your vstation_id most likely misses 'VIR'..\n"
        if 'A' not in vstation_id:
            err_msg += "Your vstation_id most likely misses 'A' (for flat) or 'A{digit}' for an aspect.."

    try:
        smet_path = f"{os.path.join(config.get('Paths', f'_snp_output_dir'), str(vstation_id))}.smet"
        smet = smet_parser.SMETParser(smet_path)
    except FileNotFoundError as e:
        error_details = err_msg + f"\nOriginal error: {str(e)}"
        raise FileNotFoundError(f"Error loading SMET file for vstation_id '{vstation_id}'. {error_details}")
    
    return smet


def upload_files(domain: str, toolchain: str, files: str, host_subdir: str):
    """Upload all files of source directory to webserver / folder"""
    if exists([toolchain, "output", "upload"], domain):
        host = os.path.join(get([toolchain, "output", "upload", "host"], domain), domain, toolchain, host_subdir)
        os.makedirs(host, exist_ok=True)
        for file in files:
            subprocess.call(["python3", _upload_script_path, file, host])
        print(f'[i]  Uploading directory "{os.path.dirname(files[0])}" to "{host}" completed.')


def progress_bar(progress_counter, lock, stime, total_tasks):
    with lock:
        pbar_interval = 5 # %
        progress_counter.value += 1
        if total_tasks <= 100/pbar_interval:
            if progress_counter.value % 2 == 0:
                print(f"[p]  Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')} - {progress_counter.value}/{total_tasks} tasks completed.")
        else:
            if (progress_counter.value % (total_tasks // (100/pbar_interval))) == 0 or progress_counter.value == total_tasks or progress_counter.value == 1:
                progress = progress_counter.value / total_tasks
                elapsed = time.time() - stime
                eta = (elapsed / progress) * (1 - progress)

                # Convert elapsed and ETA to hours, minutes, and seconds
                elapsed_hrs, elapsed_rem = divmod(elapsed, 3600)
                elapsed_min, elapsed_sec = divmod(elapsed_rem, 60)
                eta_hrs, eta_rem = divmod(eta, 3600)
                eta_min, eta_sec = divmod(eta_rem, 60)

                # Progress bar
                total_hashtags = int(100/pbar_interval)
                hashtag_str = "#" * int(np.ceil(progress * total_hashtags))
                minus_str = "-" * int((1 - progress) * total_hashtags)

                print(f"[p]  |{hashtag_str}{minus_str}| Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')} - Progress: {progress*100:05.2f}% - Elapsed: {int(elapsed_hrs):02d}:{int(elapsed_min):02d}:{int(elapsed_sec):02d} - ETA: {int(eta_hrs):02d}:{int(eta_min):02d}:{int(eta_sec):02d} (hh:mm:ss)", flush=True)
