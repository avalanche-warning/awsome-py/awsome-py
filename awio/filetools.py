################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import configparser
import glob
import itertools as it
import os
import pathlib
import time

def multiglob(*patterns):
    """Glob files for more than one pattern (e. g. multiple extensions)."""
    return it.chain.from_iterable(glob.iglob(pattern) for pattern in patterns)

def multidel(*patterns):
    files = multiglob(*patterns)
    for file in files:
        os.remove(file)

def get_scriptpath(scriptfile: str) -> str:
    """Returns the path of caller's script file.
    
    Arguments:
        scriptfile: Use __file__ for this in the calling script.
    """

    return(os.path.dirname(os.path.realpath(scriptfile)) + "/")

def is_new_file(filepath, hours=12):
    """Is creation date of file younger than some hours before?"""
    file_time = os.path.getmtime(filepath)
    delta = (time.time() - file_time) / 3600 # hours
    return delta < hours

def choose_outfile(infile: str, outfile: str=None, suffix: str=None):
    """Choose a generic outfile name depending on an input file and what's provided (file, path, nothing)."""
    infile = pathlib.Path(infile).resolve()
    if outfile:
        outfile = pathlib.Path(outfile).resolve()
        if outfile.is_dir(): # directory given --> choose file name
            stem = pathlib.Path(infile).stem
            if not suffix:
                suffix = outfile.suffix
            outfile = pathlib.Path(f'{outfile}/{stem}').with_suffix(suffix)
        else:
            if not suffix is None: # can be empty string
                outfile = outfile.with_suffix(suffix)
    else: # no name was given --> choose full path
        if not suffix:
            suffix = '.out'
        outfile = infile.with_suffix(suffix)
    return outfile

def read_credentials(authfile: str) -> tuple[str, str]:
    """Reads generic login info from file system.

    The file must contain the lines
    user=<your_username>
    password=<your_password>

    Returns:
        String tuple for username and password.
    """

    user = password = None
    fields = {}
    with open(authfile) as file:
        for line in file:
            line = line.partition("#")[0] # everything before comments
            field = line.rstrip().split("=")
            if len(field) == 2:
                fields[field[0]] = field[1]
    return(fields["user"], fields["password"])

def expanduser(config: configparser.ConfigParser):
    """
    Expand all user paths in config sections 'Paths' and 'PRIVATE'
    
    Params:
    -------
    config: configparser.ConfigParser() object

    Returns:
    --------
    config: same input object
        Modified input object with expanded user paths
    """
    sections = ['Paths', 'PRIVATE']
    sections = [section for section in sections if section in config.sections()]
    for section in sections:
        for inipath in config.options(section):
            expanded_path = os.path.expanduser(config.get(section, inipath))
            config.set(section, inipath, expanded_path)
    return config
