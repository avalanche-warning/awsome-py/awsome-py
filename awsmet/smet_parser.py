################################################################################
# Copyright 2022 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

class SMETParser:
    """A simple and efficient SMET file parser.

    This is a file parser reading SMET content in separate header and data
    containers. When instantiated as an object the full file is read and
    both sections are available. When interested in the header only the static
    methods can be called which will not read the whole file.
    The data can be requested as a pandas DataFrame also, losing some speed.

    Args:
        smetfile (str): Path to the file to parse.

    Attributes:
        header (dict): SMET header entries in a dictionary.
        data (list): SMET data entry lines in a list.
    """

    _header_lookup = "[HEADER]"
    _data_lookup = "[DATA]"
    _identifier = "SMET 1.1 ASCII"
    _time_id = "timestamp"

    def __init__(self, smetfile: str):
        self.header = {}
        self.data = []
        self.smetfile = None
        self._df = None
        self.parse_smet(smetfile)

    def parse_smet(self, smetfile: str):
        """Stores SMET header and data in separate containers.

        This function processes the whole SMET file and extracts header and data
        sections. It reads the file line by line (i. e. not all is in memory).

        Args:
            smetfile (str): Path to the file to parse.
        """

        with open(smetfile, "r") as fsmet:
            header_part = False
            data_part = False
            for line in fsmet:
                line = line.partition("#")[0].strip() # before comments
                if not line:
                    continue
                if data_part:
                    self.data.append(line)
                elif header_part:
                    if line == SMETParser._data_lookup:
                        data_part = True
                    else:
                        fields = [field.strip() for field in line.split("=")]
                        self.header[fields[0]] = fields[1]
                if line == SMETParser._header_lookup:
                    header_part = True
        self.smetfile = smetfile
        self._df = None

    def df(self, nodata: float=-999):
        """Convert the SMET data into a pandas DataFrame."""
        if not self.data:
            return None
        if self._df is None:
            import pandas as pd # only load if df is requested at least once

            self._df = pd.DataFrame(
                [ll.split() for ll in self.data],
                columns=self.get_fields()
            )
            self._df[SMETParser._time_id] = pd.to_datetime(self._df[SMETParser._time_id])
            # all columns except the timestamp are floats:
            self._df.set_index(SMETParser._time_id, inplace=True, drop=True) # promote timestamp from column to index
            self._df = self._df.replace(str(nodata), pd.NA) # here it is still an object
            self._df = self._df.apply(pd.to_numeric, errors="coerce") # strings to n/a
        return self._df

    def _write_header(self) -> str:
        """Print header to a string according to SMET specifications."""
        return SMETParser.put_header(self.header)

    def _write_data(self) -> str:
        """Print data to a string according to SMET specifications."""
        return SMETParser._data_lookup + "\n" + "\n".join(self.data)

    def append(self, other, checks=False):
        """Append data of another SMET file."""
        if checks:
            if self.get_fields() != other.get_fields():
                print(f'[E] SMET file fields do not match - can not append "{other.smetfile}" to "{self.smetfile}".')
                return
        self.data.extend(other.data)
        self._df = None

    def save(self, outfilepath=None):
        """Saves the parser's contents to the file system."""
        if not outfilepath:
            outfilepath = self.smetfile
        with open(outfilepath, "w") as outfile:
            outfile.write(self._write_header())
            outfile.write("\n" + self._write_data())

    def get_header_entry(self, key: str) -> str:
        """Returns a specific header field."""
        try:
            return self.header[key]
        except KeyError:
            return None

    def get_fields(self) -> list:
        """Retunds the header entry "fields", i. e. the contained observables."""
        return self.get_header_entry("fields").split()

    def set_header_entry(self, key: str, val):
        """Sets the value of a specific header field."""
        self.header[key] = str(val)

    def has_header_entry(self, key: str) -> bool:
        """Checks if a specific header entry is available."""
        return key in self.header

    @staticmethod
    def get_header(smetfile: str) -> dict:
        """Memory-efficient method to retrieve a SMET file header."""
        header = {}
        with open(smetfile, "r") as fsmet:
            for line in SMETParser._generate_header(fsmet):
                line = line.partition("#")[0]
                if not line:
                    continue
                fields = [field.strip() for field in line.split("=")]
                header[fields[0]] = fields[1]
        return header

    @staticmethod
    def put_header(header: dict) -> str:
        """Assembles the values of a dictionary to a string header."""
        head = SMETParser._identifier + "\n" + SMETParser._header_lookup + "\n"
        for key, val in header.items():
            head = head + key + " = " + val + "\n"
        return head.rstrip()

    @staticmethod
    def _generate_header(fsmet):
        """Generator to yield all header fields of a SMET file."""
        for _ in range(2): # skip first two lines (file identifier)
            next(fsmet) # TODO: accept comments as first lines
        for line in fsmet:
            line = line.strip()
            if line == SMETParser._data_lookup:
                break
            yield line

    @staticmethod
    def get_timespan(smetfile):
        """Gets the time span of data contained in a SMET file.

        This static method can be used if the full file does not need to be parsed.
        """

        first_data_line = last_data_line = None
        with open(smetfile, "rb") as fsmet:
            bflag = SMETParser._data_lookup.encode()
            for line in fsmet:
                if line.strip() == bflag:
                    first_data_line = fsmet.readline().decode()
                    break
            fsmet.seek(-2, 2) # jump to 2nd character from end of file
            while fsmet.read(1) != b"\n": # read 1 char and check if it's a newline
                fsmet.seek(-2, 1) # if not, jump back two chars and check again
            last_data_line = fsmet.readline().decode()

        return first_data_line.split()[0], last_data_line.split()[0]

    @staticmethod
    def write_df(df, outfilename: str, header: dict={}, nodata: float=-999):
        """Write a given pandas DataFrame as SMET file.

        This static method outputs a DataFrame to the file system as SMET.
        The header entry "fields" is added automatically, all others must be
        passed as dictionary entries.
        """
        df_reset = df.copy() # necessary to format the time stamps
        # have an ISO timestamp as 1st column and ignore index on output:
        df_reset.insert(0, SMETParser._time_id, df.index.strftime("%Y-%m-%dT%H:%M:%S"))
        with open(outfilename, "w") as outfile:
            # build header:
            head = SMETParser._identifier + "\n" + SMETParser._header_lookup + "\n"
            for key, val in header.items():
                head = head + key + " = " + val + "\n"
            outfile.write(head)
            fields = " ".join(df_reset.columns)
            outfile.write("fields = " + fields + "\n")
            outfile.write(SMETParser._data_lookup + "\n")
            # append data as "csv":
            df_reset.to_csv(
                outfile,
                sep="\t",
                na_rep=nodata,
                header=False,
                index=False,
                mode="a",
                quoting=3, #csv.QUOTE_NONE
                float_format='%.3f'
            )
